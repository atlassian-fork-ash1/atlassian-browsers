package com.atlassian.browsers;

import javax.annotation.Nonnull;

/**
 * Convenience class for implementors of {@link InstallConfigurator} who do not want to have to extend the deprecated
 * methods, or any of the methods for that matter.
 *
 * @since 2.5
 */
public abstract class AbstractInstallConfigurator extends InstallConfigurator
{

    @Override
    public void setupBrowser(@Nonnull BrowserConfig browserConfig)
    {
    }

    @Override
    public void setupFirefoxBrowser(BrowserConfig browserConfig)
    {
    }

    @Override
    public void setupChromeBrowser(BrowserConfig browserConfig)
    {
    }
}
