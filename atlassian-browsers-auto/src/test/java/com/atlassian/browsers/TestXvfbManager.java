package com.atlassian.browsers;

import org.junit.Test;

import java.io.File;

import static java.util.Arrays.asList;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class TestXvfbManager {
    private static final File TARGET_DIR = new File("target");
    private static final File TEST_TEMP_DIR = new File(TARGET_DIR, "testTmp");

    @Test
    public void xvfbListensOnTcp() {
        final XvfbManager xvfbManager = new XvfbManager(TEST_TEMP_DIR);
        try {
            xvfbManager.start();
            assertTrue("Display not found", xvfbManager.isDisplayInUse(xvfbManager.getDisplay()));
        } finally {
            xvfbManager.stop();
        }
    }

    @Test
    public void xvfbOptionsCanBeOverriden() {
        System.setProperty("xvfb.options", "foo,bar");
        try {
            final XvfbManager xvfbManager = new XvfbManager(TEST_TEMP_DIR);
            assertEquals(asList("foo", "bar"), xvfbManager.parseXvfbOptions());
        } finally {
            System.clearProperty("xvfb.options");
        }
    }

    @Test
    public void defaultOptionsIncludeOnce() {
        final XvfbManager xvfbManager = new XvfbManager(TEST_TEMP_DIR);
        assertThat(xvfbManager.parseXvfbOptions(), hasItem("-once"));
    }
}
