#!/bin/bash

set -euo pipefail

# update chrome profile, which is really just chrome driver packaged into a zip
# os, windows, osx, linux, linux64

source "$(dirname "$0")/helpers.sh"


if isHelpRequested "$*"
then
    log "$0 <version> <os> [--deploy]"
    log "  version: valid version number of the chrome driver, check out available versions at https://chromedriver.storage.googleapis.com/index.html"
    log "  os: linux64,osx,windows"
    log "  --deploy: specifies whether to do a mvn deploy:deploy-file instead of a mvn install:install-file. WARNING: this will permanently deploy given version to Maven, available to everyone"
    exit
fi

if [[ -z ${1:-} || -z ${2:-} ]]; then
    log "You need to provide an ChromeDriver version and OS. Run $0 --help for details."
    exit 1
fi

chromeVersion=$1
os=$2
deploy=0

if [[ "${3:-}" = "--deploy" ]]
then
    deploy=1

    if artifact_exists com.atlassian.browsers chrome-profile ${chromeVersion} ${os} jar; then
    	echo "Chrome Profile ${chromeVersion} (${os}) is already deployed. Skipping."
    	exit 0
    fi
fi


val=$os
if [ $val = "linux64" ]
then
	val="linux64"
elif [ $val = "osx" ]
then
    val="mac64"
elif [ $val = "windows" ]
then
    val="win32"
else
	fail "Unsupported OS: ${os}"
fi

name="/tmp/chromedriver"
url="https://chromedriver.storage.googleapis.com/${chromeVersion}/chromedriver_${val}.zip"
getFile "${name}.zip" "$url"
if [ $? != 0 ]; then
    echo "Unable to download Chrome driver from $url, aborting"
    exit 1
fi

profileDir=/tmp/chrome-profile-${os}

mkdir -p ${profileDir}

qpushd /tmp
unzip -d ${profileDir} ${name}.zip || fail "Unable to unzip ${name}.zip, likely failed to download a valid Chrome driver service binary"

echo "chrome profile version: ${chromeVersion} for ${os}" > ${profileDir}/profile.package
echo "chromedriver version: ${chromeVersion}" >> ${profileDir}/profile.package

# do preferences file if needed later.

qpushd "${profileDir}"
zipFile ${profileDir} "*"
qpopd
jarFile /tmp/chrome-${chromeVersion}-profile chrome-profile-${os}.zip
qpopd

if [ $deploy -eq 1 ]
then
    deploy_file "/tmp/chrome-${chromeVersion}-profile.jar" com.atlassian.browsers chrome-profile ${chromeVersion} ${os} jar
else
 	install_file "/tmp/chrome-${chromeVersion}-profile.jar" com.atlassian.browsers chrome-profile ${chromeVersion} ${os} jar
    log ""
    log "WARNING: This has been deployed locally to deploy to maven run with --deploy"
fi

log "Add the following to the pom.xml"
log ""
log "<dependency>"
log "  <groupId>com.atlassian.browsers</groupId>"
log "  <artifactId>chrome-profile</artifactId>"
log "  <version>${chromeVersion}</version>"
log "  <classifier>${os}</classifier>"
log "</dependency>"

## Clean up
rm /tmp/chromedriver.zip
rm -rf "${profileDir}"
rm "${profileDir}.zip"
rm /tmp/chrome-${chromeVersion}-profile.jar
