#!/bin/bash

set -euo pipefail

source "$(dirname "$0")/helpers.sh"

if isHelpRequested "$*"
then
    log "$0 <majorversion> <minorversion> [--deploy]"
    log "  Find out the version you want to download from: http://selenium-release.storage.googleapis.com/index.html"
    log "  The artifact you're looking for will be named IEDriverServer_Win32_<majorversion>.<minorversion>.zip and located in <majorversion> directory"
    log "  majorversion: e.g. 2.42"
    log "  minorversion: e.g. 2 for 2.42.2"
    log "$DEPLOY_HELP_MESSAGE"
    exit
fi

if [[ -z ${1:-} || -z ${2:-} ]]; then
    log "You need to provide an IEDriverServer major and minor version. Run $0 --help for details."
    exit 1
fi
majorVersion=$1
minorVersion=$2
version="$1.$2"

# for now we only support Windows 32bit
os=windows
val=Win32

deploy=0
if [ "${3:-}" = "--deploy" ]
then
    deploy=1

	if artifact_exists com.atlassian.browsers ie-profile ${version} ${os} jar; then
    	echo "IE Profile ${version} (${os}) is already deployed. Skipping."
    	exit 0
    fi
fi

set -u

driverfilename=IEDriverServer
driverexe="${driverfilename}.exe"
file="${driverfilename}_${val}_${version}.zip"
url="http://selenium-release.storage.googleapis.com/${majorVersion}/${file}"
name="/tmp/iedriver"

getFile "${name}.zip" "$url"
if [ $? -ne 0 ]; then
    echo "Unable to download IE Driver from $url, aborting"
    exit 1
fi

profileDirName=ie-profile-${os}
profileDir=/tmp/${profileDirName}
targetName=ie-${version}-profile
targetJar=/tmp/${targetName}.jar

mkdir -p ${profileDir}

qpushd /tmp
unzip "${name}.zip"
if [ $? -ne 0 ]; then
    echo "Unable to unzip ${name}.zip, it likely contains an error message instead of ZIP contents"
    exit 1
fi


cp "/tmp/${driverexe}" "${profileDir}"

version=`echo $file | egrep -o "[0-9.]+.zip" | sed -e 's/\.zip//'`
echo "IE profile version: ${version} for ${os}" > ${profileDir}/profile.package
echo "${driverfilename} version: ${version}" >> ${profileDir}/profile.package

# do preferences file if needed later.

qpushd "${profileDir}"
zipFile "${profileDir}" "*"
qpopd
jarFile /tmp/${targetName} ${profileDirName}.zip
qpopd

if [ $deploy -eq 1 ]
then
    deploy_file "${targetJar}" com.atlassian.browsers ie-profile ${version} ${os} jar
else
    install_file "${targetJar}" com.atlassian.browsers ie-profile ${version} ${os} jar
    log ""
    log "WARNING: This has been deployed locally to deploy to maven run with --deploy"
fi

log "Add the following to the pom.xml"
log ""
log "<dependency>"
log "  <groupId>com.atlassian.browsers</groupId>"
log "  <artifactId>ie-profile</artifactId>"
log "  <version>${version}</version>"
log "  <classifier>${os}</classifier>"
log "</dependency>"

## Clean up
rm "/tmp/${driverexe}"
rm "${name}.zip"
rm -rf "${profileDir}"
rm "${profileDir}.zip"
rm "${targetJar}"
