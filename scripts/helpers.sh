#!/bin/bash

set -euo pipefail

log () { echo -e "$*" >&2; return $? ; }

fail () { log "\nERROR: $*\n" ; exit 1 ; }

qpushd () {
    pushd "$1" &> /dev/null
}

qpopd () {
    popd &> /dev/null
}

cmd_exists () {
  type "$1" &> /dev/null;
}

getFile () {
    local output="$1"
    local url="$2"

    log "Retrieving file ${output} from ${url}"

    if cmd_exists wget
    then
        wget --quiet -O "$output" "$url"
    else
        curl --silent -o "$output" "$url"
    fi
    return $?
}

jarFile () {
    local name=$1
    local path=$2
    log "Creating zip for ${name}"
    if cmd_exists jar
    then
       jar Mcf "${name}.jar" ${path}
    else
       fail "jar command not found."
    fi

}

zipFile () {
    local name=$1
    local path=$2
    if cmd_exists zip; then
    	zip -q -r "${name}.zip" ${path}
   	else
   		fail "zip command not found."
   	fi
}

isHelpRequested() {
    for arg in $*
    do
        if [[ "$arg" = "help" || "$arg" = "-help" || "$arg" = "--help" || "$arg" = "-h" ]]
        then
            return 0
        fi
    done
    return 1
}

get_mvn_prop() {
    mvn -N -f "$(dirname "$0")/../pom.xml" -q exec:exec -Dexec.executable=echo -Dexec.args="\${$@}"
}

artifact_exists() {
	local group="$1"
    local artifact="$2"
    local version="$3"
    local classifier="${4-}"
    local extension="${5:-jar}"

    local artifactId="$group:$artifact:$version:$extension${classifier:+":$classifier"}"
    local resource="$(tr . / <<<"$group")/$artifact/$version/$artifact-$version${classifier:+"-$classifier"}.$extension"
    local output

    log "--- Checking whether $artifactId already exists on packages.atlassian.com..."

    if ! output="$(mvn -e -N org.codehaus.mojo:wagon-maven-plugin:2.0.0:exist -f "$(dirname "$0")/../pom.xml" -Dwagon.serverId="${MAVEN_REPO_ID}" -Dwagon.url="${MAVEN_REPO_URL}" -Dwagon.resource="$resource")"; then
        log "!!! Failed to execute maven command:"
        log "$output"
        exit 1
    fi

    if grep "$resource exists." <<<"$output" >/dev/null; then
        log ">>> $artifactId already exists"
    else
        log ">>> $artifactId does not exist"
        return 1
    fi
}

deploy_file() {
    local file="$1"
    local group="$2"
    local artifact="$3"
    local version="$4"
    local classifier="${5-}"
    local extension="${6:-jar}"

    log "=== Deploying file '$(basename "$file")'"

    # The 'restricted' repository on artifactory doesn't allow overwriting of existing pom - just skip it in that case
    local generate_pom=true
    if artifact_exists "$group" "$artifact" "$version" "" "pom" 2>/dev/null; then
        log '--- Skipping publishing of POM since it already exists'
        generate_pom=false
    fi

    mvn -N -e deploy:deploy-file -Durl="${MAVEN_REPO_URL}" \
                              -DrepositoryId="${MAVEN_REPO_ID}" \
                              -Dfile="$file" \
                              -DgroupId="$group" \
                              -DartifactId="$artifact" \
                              -Dversion="$version" \
                              ${classifier:+"-Dclassifier=$classifier"} \
                              -Dpackaging="${extension}" \
                              -DgeneratePom="$generate_pom"
}

install_file() {
    local file="$1"
    local group="$2"
    local artifact="$3"
    local version="$4"
    local classifier="${5-}"
    local extension="${6:-jar}"

    log "=== Installing file '$(basename "$file")'"

    mvn -N -e install:install-file \
                              -Dfile="$file" \
                              -DgroupId="$group" \
                              -DartifactId="$artifact" \
                              -Dversion="$version" \
                              ${classifier:+"-Dclassifier=$classifier"} \
                              -Dpackaging="${extension}"
}

MAVEN_REPO_URL=https://packages.atlassian.com/maven/public
MAVEN_REPO_ID=atlassian-public

DEPLOY_HELP_MESSAGE="  --deploy: specifies whether to do a mvn deploy:deploy-file instead of a mvn install:install-file. WARNING: this will permanently deploy that version to Maven, available to everyone. Only use this after making sure your artifact is valid."
CHROME_VERSION_WARNING="WARNING: for Chrome the latest stable browser will be retrieved, so make sure to provide the latest stable version."
